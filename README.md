**Member :**

Ilham Pratama (4210181020)
Ilham Agung Riyadi (4210181023)
Dicky Dwi Darmawan (4210181028)

**Description : **

Client Server Calculator Program using Multithreading TCP connection.
* create Main func and create listener and Ip parse.
* then loop the programs using while true.
* after clients accept connection, create new thread conn.
* then start the thread.
* after thread started, make new func to operate the calculator program using code in Source Code.
* then return the connection to the Clients.
* Client will receive result of number to calculate.