//member :

// Ilham Pratama (4210181020)
// Ilham Agung Riyadi (4210181023)
// Dicky Dwi Darmawan (4210181028)


using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.IO;

namespace server
{
    class Program
    {
        private static void ProcessClientRequests(object argument)
        {
            TcpClient client = (TcpClient)argument;
            try {

			StreamReader reader = new StreamReader(client.GetStream());
			StreamWriter writer = new StreamWriter(client.GetStream());

            int operation = 0;
            double result = 0;

			while(true) {
			
            string c = reader.ReadLine();
            double firstNumber = Convert.ToDouble(c);
            Console.WriteLine("client first number: {0}", c);

            string d = reader.ReadLine();
            Console.WriteLine("your operand: {0}", d);

            string e = reader.ReadLine();
            double secondNumber = Convert.ToDouble(e);
            Console.WriteLine("client second number: {0}", e);

            if (d == "+" || d == "addition")
            {
                operation = 1;
            }
            else if (d == "-" || d == "soustraction")
            {
                operation = 2;
            }
            else if (d == "*" || d == "multiplication")
            {
                operation = 3;
            }
            else if (d == "/" || d == "division")
            {
                operation = 4;
            }
            else if (d == "^" || d == "exposant")
            {
                operation = 5;
            }
            else if (d == "%" || d == "reste")
            {
                operation = 6;
            }

            switch (operation)
            {
                case 1:
                    result = firstNumber + secondNumber;
                    break;

                case 2:
                    result = firstNumber - secondNumber;
                    break;

                case 3:
                    result = firstNumber * secondNumber;
                    break;

                case 4:
                    result = firstNumber / secondNumber;
                    break;

                case 5:
                    result = Math.Pow(firstNumber, secondNumber);
                    break;

                case 6:
                    result = firstNumber % secondNumber;
                    break;
            }
            Console.WriteLine("\nResult of " + firstNumber + " " + d + " " + secondNumber + " = " + result + ".");
            string hasil = Convert.ToString(result);
            writer.WriteLine("Result : " + hasil);
            writer.Flush();
            Console.WriteLine();
        	}
            reader.Close();
			writer.Close();
			client.Close();
			Console.WriteLine("Closing client connection!");
            }
            catch(IOException){
                Console.WriteLine("Problem with client communication. Exiting thread.");
                } 
            finally{
			if(client != null){
                client.Close();
                }
            }
        }
    

        public static void Main(){
		TcpListener listener = null;
		try {
			listener = new TcpListener(IPAddress.Parse("127.0.0.1"), 8080);
			listener.Start();
			Console.WriteLine("MultiThreadedEchoServer started...");
			while(true){
				Console.WriteLine("Waiting for incoming client connections...");
				TcpClient client = listener.AcceptTcpClient();
				Console.WriteLine("Accepted new client connection...");
				Thread t = new Thread(ProcessClientRequests);
				t.Start(client);
			}
		} catch(Exception e){
				Console.WriteLine(e);
		} finally{
				if(listener != null){
					listener.Stop();
				}
		    }
        }
    }
}